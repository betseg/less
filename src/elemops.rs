use crate::Matrix;

use num_rational::Rational;

impl Matrix {
    pub fn is_echeleon(&self) -> bool {
        let mut cur_ix: usize = 0;

        for n in 0..self.width {
            let temp: usize;
            if let Some(pos) = self.coeffs[n * self.width..(n + 1) * self.width]
                .iter()
                .position(|&x| x != Rational::from_integer(0))
            {
                temp = pos;
            } else {
                temp = self.width;
            }

            if temp < cur_ix {
                return false;
            }
            cur_ix = temp;
        }

        true
    }

    pub fn switch(&mut self, r1: usize, r2: usize) {
        if r1 < self.height && r2 < self.height {
            for i in 0..self.width {
                self.coeffs.swap(r1 * self.width + i, r2 * self.width + i);
            }
            self.consts.swap(r1, r2);
        }
    }

    pub fn mult(&mut self, i: usize, k: Rational) {
        if i < self.height && k != Rational::from_integer(0) {
            for n in 0..self.width {
                self[(n, i)] *= k;
            }
            self[i] *= k;
        }
    }

    pub fn add(&mut self, i: usize, j: usize, k: Rational) {
        if i != j && k != Rational::from_integer(0) && i < self.height && j < self.height {
            for n in 0..self.width {
                let val = self[(n, j)];
                self[(n, i)] += val * k;
            }
            let val = self[j];
            self[i] += val * k;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn echeleon() {
        assert!(Matrix::new_unit(3).is_echeleon());
        assert!(Matrix::new(3, 5).is_echeleon());

        let n_ech = Matrix::from_ints(&[1, 1, 1, 0, 2, 2, 3, 3, 3], &[0, 0, 0], 3, 3);
        let ech = Matrix::from_ints(&[1, 1, 1, 0, 2, 2, 0, 0, 3], &[0, 0, 0], 3, 3);

        assert!(!n_ech.is_echeleon());
        assert!(ech.is_echeleon());
    }

    #[test]
    fn switch() {
        let unsw = Matrix::from_ints(&[1, 1, 1, 2, 2, 2, 3, 3, 3], &[1, 2, 3], 3, 3);

        let mut sw = Matrix::from_ints(&[2, 2, 2, 1, 1, 1, 3, 3, 3], &[2, 1, 3], 3, 3);
        sw.switch(0, 1);

        assert_eq!(unsw, sw);
    }

    mod mult {
        use super::*;

        #[test]
        fn mult() {
            let res = Matrix::from_ints(&[3, 6, 9, 4, 5, 6, 7, 8, 9], &[3, 2, 3], 3, 3);

            let mut mult = Matrix::from_ints(&[1, 2, 3, 4, 5, 6, 7, 8, 9], &[1, 2, 3], 3, 3);
            mult.mult(0, Rational::from_integer(3));

            assert_eq!(res, mult);
        }

        #[test]
        fn mult_div() {
            let res = Matrix::from_rats(
                &[
                    Rational::new(1, 3),
                    Rational::new(2, 3),
                    Rational::new(3, 3),
                    Rational::from_integer(4),
                    Rational::from_integer(5),
                    Rational::from_integer(6),
                    Rational::from_integer(7),
                    Rational::from_integer(8),
                    Rational::from_integer(9),
                ],
                &[
                    Rational::new(1, 3),
                    Rational::from_integer(2),
                    Rational::from_integer(3),
                ],
                3,
                3,
            );

            let mut mult = Matrix::from_ints(&[1, 2, 3, 4, 5, 6, 7, 8, 9], &[1, 2, 3], 3, 3);
            mult.mult(0, Rational::new(1, 3));

            assert_eq!(res, mult);
        }
    }

    mod add {
        use super::*;

        #[test]
        fn add_one() {
            let res = Matrix::from_ints(&[5, 7, 9, 4, 5, 6, 7, 8, 9], &[3, 2, 3], 3, 3);

            let mut add = Matrix::from_ints(&[1, 2, 3, 4, 5, 6, 7, 8, 9], &[1, 2, 3], 3, 3);
            add.add(0, 1, Rational::from_integer(1));

            assert_eq!(res, add);
        }

        #[test]
        fn add_two() {
            let res = Matrix::from_ints(&[9, 12, 15, 4, 5, 6, 7, 8, 9], &[5, 2, 3], 3, 3);

            let mut add = Matrix::from_ints(&[1, 2, 3, 4, 5, 6, 7, 8, 9], &[1, 2, 3], 3, 3);
            add.add(0, 1, Rational::new(2, 1));

            assert_eq!(res, add);
        }

        #[test]
        fn add_half() {
            let res = Matrix::from_rats(
                &[
                    Rational::new(6, 2),
                    Rational::new(9, 2),
                    Rational::new(12, 2),
                    Rational::from_integer(4),
                    Rational::from_integer(5),
                    Rational::from_integer(6),
                    Rational::from_integer(7),
                    Rational::from_integer(8),
                    Rational::from_integer(9),
                ],
                &[
                    Rational::from_integer(2),
                    Rational::from_integer(2),
                    Rational::from_integer(3),
                ],
                3,
                3,
            );
            let mut add = Matrix::from_ints(&[1, 2, 3, 4, 5, 6, 7, 8, 9], &[1, 2, 3], 3, 3);

            add.add(0, 1, Rational::new(1, 2));

            assert_eq!(res, add);
        }
    }
}
