use crate::Matrix;

use num_rational::Rational;

use std::fmt;
use std::ops::{Index, IndexMut};

impl fmt::Display for Matrix {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for y in 0..self.height {
            for x in 0..self.width {
                write!(f, "{} ", self[(x, y)])?;
            }
            writeln!(f, "| {}", self[y])?;
        }
        Ok(())
    }
}

impl Index<(usize, usize)> for Matrix {
    type Output = Rational;

    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        match (x, y) {
            (x, y) if x < self.width && y < self.height => &self.coeffs[y * self.width + x],
            _ => panic!(),
        }
    }
}

impl IndexMut<(usize, usize)> for Matrix {
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut Self::Output {
        match (x, y) {
            (x, y) if x < self.width && y < self.height => &mut self.coeffs[y * self.width + x],
            _ => panic!(),
        }
    }
}

impl Index<usize> for Matrix {
    type Output = Rational;

    fn index(&self, index: usize) -> &Self::Output {
        match index {
            y if y < self.height => &self.consts[y],
            _ => panic!(),
        }
    }
}

impl IndexMut<usize> for Matrix {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match index {
            y if y < self.height => &mut self.consts[y],
            _ => panic!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn index_coeff() {
        let zero = Rational::from_integer(0);
        let one = Rational::from_integer(1);

        let uwu = Matrix::new_unit(3);
        assert_eq!(one, uwu[(0, 0)]);
        assert_eq!(zero, uwu[(0, 1)]);
        assert_eq!(one, uwu[(1, 1)]);
        assert_eq!(zero, uwu[(1, 2)]);
    }

    #[test]
    fn index_mut_coeff() {
        let one = Rational::from_integer(1);

        let mut uwu = Matrix::new(3, 3);
        uwu[(0, 0)] = one;
        uwu[(1, 1)] = one;
        assert_eq!(one, uwu[(0, 0)]);
        assert_eq!(one, uwu[(1, 1)]);
    }

    #[test]
    fn index_const() {
        let one = Rational::from_integer(1);
        let two = Rational::from_integer(2);

        let uwu = Matrix::from_ints(&[1, 1, 1, 1], &[1, 2], 2, 2);
        assert_eq!(one, uwu[0]);
        assert_eq!(two, uwu[1]);
    }

    #[test]
    fn index_mut_const() {
        let one = Rational::from_integer(1);
        let two = Rational::from_integer(2);

        let mut uwu = Matrix::from_ints(&[1, 1, 1, 1], &[0, 0], 2, 2);
        uwu[0] = one;
        uwu[1] = two;
        assert_eq!(one, uwu[0]);
        assert_eq!(two, uwu[1]);
    }
}
