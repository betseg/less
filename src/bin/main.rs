use linearsystems::Matrix;

fn main() {
    let mut matrix = Matrix::from_ints(&[1, 1, 1, 1, -2, 3, 3, -1, 2], &[6, 6, 7], 3, 3);

    println!("{}", matrix);

    matrix.solve();

    println!("{}", matrix);
}
