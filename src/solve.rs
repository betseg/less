use crate::Matrix;

use num_rational::Rational;

impl Matrix {
    pub fn solve(&mut self) {
        let zero = Rational::from_integer(0);
        let one = Rational::from_integer(1);

        for i in 0..self.height {
            for j in i + 1..self.height {
                let i_val = self[(i, i)];
                let j_val = self[(i, j)];

                if i_val != zero && j_val != zero {
                    self.add(j, i, -j_val / i_val);
                }
            }
        }

        for i in (0..self.height).rev() {
            for j in (i + 1..self.height).rev() {
                let i_val = self[(j, i)];
                let j_val = self[(j, j)];

                if i_val != zero {
                    self.add(i, j, -i_val / j_val);
                }
            }
        }

        for i in 0..self.height {
            let val = self[(i, i)];
            self.mult(i, one / val);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn solvetest1() {
        let mut matrix = Matrix::from_ints(&[1, 1, 1, 1, -2, 3, 3, -1, 2], &[6, 6, 7], 3, 3);

        matrix.solve();

        assert_eq!(
            matrix.get_consts(),
            vec![
                Rational::from_integer(1),
                Rational::from_integer(2),
                Rational::from_integer(3)
            ]
        );
    }

    #[test]
    fn solvetest2() {
        let mut matrix = Matrix::from_ints(&[2, 1, -1, -3, -1, 2, -2, 1, 2], &[8, -11, -3], 3, 3);

        matrix.solve();

        assert_eq!(
            matrix.get_consts(),
            vec![
                Rational::from_integer(2),
                Rational::from_integer(3),
                Rational::from_integer(-1)
            ]
        );
    }
}
