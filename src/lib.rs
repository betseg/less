use num_rational::{Ratio, Rational};

mod elemops;
mod solve;
mod utils;

#[derive(PartialEq, Debug)]
pub struct Matrix {
    coeffs: Vec<Rational>,
    consts: Vec<Rational>,
    width: usize,
    height: usize,
}

impl Matrix {
    pub fn new(width: usize, height: usize) -> Self {
        Self {
            coeffs: vec![Rational::from_integer(0); width * height],
            consts: vec![Rational::from_integer(0); height],
            width,
            height,
        }
    }

    pub fn from_ints(coeffs: &[isize], consts: &[isize], width: usize, height: usize) -> Self {
        let coeffs: Vec<_> = coeffs.iter().map(|&e| Ratio::from_integer(e)).collect();
        let consts: Vec<_> = consts.iter().map(|&e| Ratio::from_integer(e)).collect();
        Self {
            coeffs,
            consts,
            width,
            height,
        }
    }

    pub fn from_rats(
        coeffs: &[Rational],
        consts: &[Rational],
        width: usize,
        height: usize,
    ) -> Self {
        Self {
            coeffs: coeffs.into(),
            consts: consts.into(),
            width,
            height,
        }
    }

    pub fn new_unit(size: usize) -> Self {
        let mut res = Self {
            coeffs: vec![Rational::from_integer(0); size * size],
            consts: vec![Rational::from_integer(0); size],
            width: size,
            height: size,
        };
        for i in 0..size {
            res[(i, i)] = Rational::from_integer(1);
        }
        res
    }

    pub fn get_consts(&self) -> Vec<Rational> {
        self.consts.clone()
    }

    pub fn is_unit(&self) -> bool {
        if self.width != self.height {
            return false;
        }
        for i in 0..self.height {
            for j in 0..self.width {
                if (i != j && self[(i, j)] != Rational::from_integer(0))
                    || (i == j && self[(i, j)] != Rational::from_integer(1))
                {
                    return false;
                }
            }
        }
        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn eq() {
        let uwu = Matrix::new(3, 4);
        let owo = Matrix::new(3, 4);
        assert_eq!(uwu, owo);
        let uwu = Matrix::new(3, 4);
        let owo = Matrix::new(4, 3);
        assert_ne!(uwu, owo);
    }

    #[test]
    fn from() {
        let zero = Rational::from_integer(0);
        let one = Rational::from_integer(1);

        let unit = Matrix::new_unit(3);
        let ints = Matrix::from_ints(&[1, 0, 0, 0, 1, 0, 0, 0, 1], &[0, 0, 0], 3, 3);
        let rats = Matrix::from_rats(
            &[one, zero, zero, zero, one, zero, zero, zero, one],
            &[zero, zero, zero],
            3,
            3,
        );
        assert_eq!(unit, ints);
        assert_eq!(unit, rats);
    }

    #[test]
    fn bounds() {
        let zero = Rational::from_integer(0);

        let uwu = Matrix::new(3, 4);
        assert_eq!(zero, uwu[(0, 0)]);
        assert_eq!(zero, uwu[(2, 3)]);
    }

    #[test]
    #[should_panic]
    fn panic1() {
        let uwu = Matrix::new(3, 4);
        uwu[(2, 4)];
    }
    #[test]
    #[should_panic]
    fn panic2() {
        let uwu = Matrix::new(3, 4);
        uwu[(3, 3)];
    }
    #[test]
    #[should_panic]
    fn panic3() {
        let uwu = Matrix::new(3, 4);
        uwu[(3, 4)];
    }
    #[test]
    #[should_panic]
    fn panic4() {
        let uwu = Matrix::new(3, 4);
        uwu[(4, 5)];
    }

    #[test]
    fn unit() {
        let unit_three = Matrix::new_unit(3);
        let three = Matrix::from_ints(&[1, 0, 0, 0, 1, 0, 0, 0, 1], &[0, 0, 0], 3, 3);
        assert_eq!(unit_three, three);
    }

    #[test]
    fn is_unit() {
        assert!(Matrix::new_unit(3).is_unit());
        assert!(!Matrix::new(3, 3).is_unit());

        let unit = Matrix::from_ints(&[1, 0, 0, 0, 1, 0, 0, 0, 1], &[0, 0, 0], 3, 3);
        assert!(unit.is_unit());
    }
}
